import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Account from 'App/Models/Account';
import Role from 'App/Models/Role';

export default class extends BaseSeeder {
  public async run() {
    const emp = await Role.findBy('name', 'emp')
    const hrd = await Role.findBy('name', 'hrd')

    const acc = await Account.findBy('email', 'muhamad.fajar@argon-group.com')

    if (acc == null) {
      await Account.createMany([
        {
          email: 'muhamad.fajar@argon-group.com',
          password: '12345678',
          phone: '628221700',
          roleID: emp ? emp.id : 0,
          firstName: 'Muhamad',
          lastName: 'Fajar',
          profilePic: 'https://cdn.muhfajar.id/pub/img/default_avatar.png', // default avatar
          title: 'Full-Stack Developer Lead'
        },
        {
          email: 'hrd.account@argon-group.com',
          password: '12345678',
          phone: '628221701',
          roleID: hrd ? hrd.id : 0,
          firstName: 'HRD',
          lastName: 'Argon',
          profilePic: 'https://cdn.muhfajar.id/pub/img/default_avatar.png', // default avatar
          title: 'HRD'
        }
      ])
    }
  }
}
