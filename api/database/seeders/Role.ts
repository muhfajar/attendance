import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Role from 'App/Models/Role';

export default class extends BaseSeeder {
  public async run() {
    const emp = await Role.findBy('name', 'emp')

    if (emp == null) {
      await Role.createMany([
        {
          name: 'hrd',
        },
        {
          name: 'emp'
        }
      ])
    }
  }
}
