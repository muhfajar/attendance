import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import AttendanceStatus from 'App/Models/AttendanceStatus';

export default class extends BaseSeeder {
  public async run () {
    await AttendanceStatus.createMany([
      {
        name: 'start'
      },
      {
        name: 'end'
      },
      {
        name: 'break'
      }
    ])
  }
}
