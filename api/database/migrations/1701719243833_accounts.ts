import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'accounts'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('email').unique()
      table.string('password')
      table.string('phone').unique()
      table.integer('role_id')
      table.string('first_name')
      table.string('last_name')
      table.string('profile_pic')
      table.string('title')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', {useTz: true})
      table.timestamp('updated_at', {useTz: true})
    })

    this.schema.alterTable(this.tableName, (table) => {
      table.foreign('role_id').references('roles.id')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
