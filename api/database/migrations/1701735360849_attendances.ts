import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class extends BaseSchema {
  protected tableName = 'attendances'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('account_id')
      table.date('work_date')
      table.timestamp('work_time')
      table.integer('status_id')

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })

    this.schema.alterTable(this.tableName, (table) => {
      table.foreign('account_id').references('accounts.id')
      table.foreign('status_id').references('attendance_statuses.id')
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
