import {HttpContextContract} from '@ioc:Adonis/Core/HttpContext';
import minioClient from 'Config/minio';
import Env from '@ioc:Adonis/Core/Env';
import Application from '@ioc:Adonis/Core/Application';
import Account from 'App/Models/Account';

export default class UpdateEmployeeDataController {
  public async handle({request}: HttpContextContract): Promise<any> {
    if (request.ctx && request.ctx.auth && request.ctx.auth.user) {
      const userID = request.ctx.auth.user.id

      // Update profile picture
      async function updateProfilePicture(): Promise<any> {
        const profilePic = request.file('profile_pic')

        if (profilePic) {
          await profilePic.moveToDisk(Application.tmpPath('uploads'))

          // Upload profile picture to Minio
          const bucket = Env.get('MINIO_BUCKET')
          const metaData = {
            'Content-Type': 'application/octet-stream',
          };

          if (profilePic.fileName && profilePic.filePath) {
            await minioClient.fPutObject(bucket, profilePic.fileName, profilePic.filePath, metaData)

            return profilePic.fileName
          }
        }
      }

      const fileName = await updateProfilePicture();
      const profilePictureURL = `${Env.get('MINIO_BASE_PATH')}${fileName}`

      const account = await Account.findOrFail(userID)

      if (request.file('profile_pic')) {
        account.profilePic = profilePictureURL
      }

      if (request.input('phone')) {
        account.phone = request.input('phone')
      }

      if (request.input('password') && request.input('password') == request.input('retype-password')) {
        account.password = request.input('password')
      }

      await account.save()

      return account.serialize({
        fields: {
          omit: ['password', 'created_at', 'updated_at']
        }
      })
    }
  }
}
