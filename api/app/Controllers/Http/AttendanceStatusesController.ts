import AttendanceStatus from 'App/Models/AttendanceStatus';
import Redis from "@ioc:Adonis/Addons/Redis";

export default class AttendanceStatusesController {
  public async handle() {
    const key = "attendee_status_list"
    const list = await Redis.lrange(key, 0, -1)

    if (list.length > 0) {
      return list
    } else {
      const status = await AttendanceStatus.all()
      const list = status.map((list) => list.name)

      await Redis.lpush(key, ...list)
      await Redis.expire(key, 15)

      return list
    }
  }
}
