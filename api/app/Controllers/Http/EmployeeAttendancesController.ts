import type {HttpContextContract} from '@ioc:Adonis/Core/HttpContext'
import Attendance from 'App/Models/Attendance';
import AttendanceStatus from 'App/Models/AttendanceStatus';

export default class EmployeeAttendancesController {
  public async handle({request}: HttpContextContract): Promise<any> {
    if (request.ctx && request.ctx.auth && request.ctx.auth.user) {
      const userID = request.ctx.auth.user.id

      const status = await AttendanceStatus.findBy('name', request.input('status'))
      const statusID = status?.id

      const { DateTime } = require('luxon');
      const date = DateTime.local();

      const attendance = await Attendance.create({
        accountID: userID,
        statusID: statusID,
        workDate: date,
        workTime: date.toString(),
      })

      return attendance.serialize({
        fields: {
          pick: ['work_date', 'work_time']
        }
      })
    }
  }
}
