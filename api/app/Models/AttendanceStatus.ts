import {DateTime} from 'luxon'
import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'

export default class AttendanceStatus extends BaseModel {
  @column({isPrimary: true})
  public id: number

  @column()
  public name: string

  @column.dateTime({columnName: 'created_at', autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({columnName: 'updated_at', autoCreate: true, autoUpdate: true})
  public updatedAt: DateTime
}
