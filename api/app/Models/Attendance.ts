import {DateTime} from 'luxon'
import {BaseModel, column} from '@ioc:Adonis/Lucid/Orm'
import Account from 'App/Models/Account';
import {hasOne} from '@adonisjs/lucid/build/src/Orm/Decorators';

export default class Attendance extends BaseModel {
  @column({isPrimary: true})
  public id: number

  @column({columnName: 'account_id'})
  @hasOne(() => Account)
  // @ts-ignore
  public accountID: hasOne<typeof Account>

  @column.date({columnName: 'work_date'})
  public workDate: DateTime

  @column.date({columnName: 'work_time'})
  public workTime: DateTime

  @column({columnName: 'status_id'})
  public statusID: number

  @column.dateTime({columnName: 'created_at', autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({columnName: 'updated_at', autoCreate: true, autoUpdate: true})
  public updatedAt: DateTime
}
