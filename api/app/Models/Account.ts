import {DateTime} from 'luxon'
import {afterUpdate, BaseModel, beforeSave, column} from '@ioc:Adonis/Lucid/Orm'
import {hasOne} from '@adonisjs/lucid/build/src/Orm/Decorators';
import Role from 'App/Models/Role';
import {HasOne} from '@adonisjs/lucid/build/src/Orm/Relations/HasOne';
import Hash from '@ioc:Adonis/Core/Hash'
import Redis from '@ioc:Adonis/Addons/Redis'

export default class Account extends BaseModel {
  @column({isPrimary: true})
  public id: number

  @column()
  public email: string

  @column()
  public password: string

  @column()
  public phone: string

  @column({columnName: 'role_id'})
  @hasOne(() => Role)
  // @ts-ignore
  public roleID: HasOne<typeof Role>

  @column({columnName: 'first_name'})
  public firstName: string

  @column({columnName: 'last_name'})
  public lastName: string

  @column({columnName: 'profile_pic'})
  public profilePic: string

  @column()
  public title: string

  @column.dateTime({columnName: 'created_at', autoCreate: true})
  public createdAt: DateTime

  @column.dateTime({columnName: 'updated_at', autoCreate: true, autoUpdate: true})
  public updatedAt: DateTime

  @beforeSave()
  public static async hashPassword(account: Account) {
    if (account.$dirty.password) {
      account.password = await Hash.make(account.password)
    }
  }

  @afterUpdate()
  public static async publishEvent(account: Account) {
    await Redis.publish('account:update', JSON.stringify({
      id: account.id,
      email: account.email,
      name: `${account.firstName} ${account.lastName}`
    }))
  }
}
