/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'
import AutoSwagger from 'adonis-autoswagger';
import swagger from 'Config/swagger';

Route
  .group(() => {
    // returns swagger in YAML
    Route.get('swagger', async () => {
      // @ts-ignore
      return AutoSwagger.docs(Route.toJSON(), swagger);
    });

    // Renders Swagger-UI and passes YAML-output of /swagger
    Route.get('docs', async () => {
      return AutoSwagger.ui('/debug/swagger');
    });
  })
  .prefix('/debug')

Route
  .group(() => {
    Route.post('login', async ({auth, request}) => {
      const email = request.input('email')
      const password = request.input('password')

      const token = await auth.use('api').attempt(email, password, {
        expiresIn: '30 minutes'
      })

      return token.toJSON()
    })

    Route.post('logout', async ({auth}) => {
      await auth.use('api').revoke()
      return {
        revoked: true
      }
    })
  })
  .prefix('/auth')

Route
  .group(() => {
    Route.put('/', 'UpdateEmployeeDataController')
    Route.post('/attendances', 'EmployeeAttendancesController')
  })
  .prefix('/v1/employee')
  .middleware('auth')

Route
  .group(() => {
    Route.get('/attendee_status', 'AttendanceStatusesController')
  })
  .prefix('/v1/static')
  .middleware('auth')
