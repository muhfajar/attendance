import * as Minio from 'minio'
import Env from "@ioc:Adonis/Core/Env";

const portString = Env.get('MINIO_PORT');
const port = parseInt(portString, 10);

function parseBool(value: string) : boolean {
  return value.toLowerCase() === 'true';
}

const sslString = Env.get('MINIO_SSL');
const ssl = parseBool(sslString);

const minioClient = new Minio.Client({
  endPoint: Env.get('MINIO_HOST'),
  port: port,
  useSSL: ssl,
  accessKey: Env.get('MINIO_ACCESS_KEY'),
  secretKey: Env.get('MINIO_SECRET_KEY'),
})

export default minioClient
