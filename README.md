# Absensi

---

### Change log
- Containerize project
- Trigger publish on account model; notify changes via redis pub/sub
- Upload avatar using minio

## Prerequisite
- Docker
- Compose

## Local Development
- All process can be done using compose command `docker compose up`
- Login to Minio dashboard with credential: `user: minio-root-user; pass: minio-root-password`
- Create Minio bucket `dexa-minio` from minio dashboard `http://127.0.0.1:9001/buckets/add-bucket`
- Create Minio API access key `http://127.0.0.1:9001/access-keys/new-account` with detail `key: minio-access-key; secret: minio-secret-key`
- Migrate db schema `docker exec dexa_api sh -c "node ace migration:run"`
- Run db seed to prefilled the default data `docker exec dexa_api sh -c "node ace db:seed"`

## API Docs
- All endpoint listed in Swagger docs `http://127.0.0.1:3333/docs`

## Default Env
```dotenv
PORT=3333
HOST=0.0.0.0
NODE_ENV=development
APP_KEY=tewlIY_LgY90SSMvP89G8GHweR8MeYlg
DRIVE_DISK=local

MINIO_HOST=minio
MINIO_PORT=9000
MINIO_SSL=false
MINIO_ACCESS_KEY=minio-access-key
MINIO_SECRET_KEY=minio-secret-key
MINIO_BUCKET=dexa-minio
MINIO_BASE_PATH=http://127.0.0.1:9000/dexa-minio/

DB_CONNECTION=pg
PG_HOST=postgres
PG_PORT=5432
PG_USER=postgres
PG_PASSWORD=postgres
PG_DB_NAME=postgres

REDIS_CONNECTION=local
REDIS_HOST=redis
REDIS_PORT=6379
REDIS_PASSWORD=
```
