// @ts-ignore
import type {Metadata} from 'next'

// @ts-ignore
import {Inter} from 'next/font/google'

import './globals.css'
import React from "react";

const inter = Inter({subsets: ['latin']})

export const metadata: Metadata = {
    title: 'Sistem Absensi',
    description: 'Muhamad Fajar',
}

export default function RootLayout({children}: {
    children: React.ReactNode
}) {
    return (
        <html lang="en">
        <body className={inter.className}>{children}</body>
        </html>
    )
}
